Salut beta-testeur,
la doc de SMILE n'est pas tres fournie, mais voici un rapide apprentissage par
l'exemple.

SMILE permet d'extraire des modeles structures ayant des caracteristiques
precises d'un jeu de sequences donne, et d'evaluer la representativite des
modeles trouves selon deux methodes.

Le seul programme que tu utiliseras est "smile". On lui donne en parametre
un fichier contenant les caracteristiques des modeles a extraire.
Le fichier "param_exemple" est comment� et te donneras un apercu des
caracteristiques de base. Les fichiers "param_1bloc", "param_contre"
et "param_delta" montrent des options supplementaires.

L'execution de SMILE se fait en deux phases independantes:
    - extraction des modeles repondant aux criteres donnes
    - evaluation de ces modeles sur des criteres statistiques, selon
      deux methodes au choix (shuffling et contre autres sequences).

L'execution de "smile param_exemple" (voir le fichier param_exemple pour
connaitre les criteres d'extraction) produit:
  1 - le fichier "example.out" qui contient tous les modeles repondant aux
      criteres demandes. En voici un extrait:

            AAAAAA_TGAAAA 000000-320000 44
            Seq   433   Pos   187	18 
            Seq   544   Pos   213	17 
            ...
            Seq   438   Pos    81	19 
            Seq   931   Pos   165	18 
            63

      Le modeles AAAAAA_TGAAAA apparait dans 44 sequences en 63 positions.
      Puis, chaque position est indiquee. Les sequences et positions commencent
      a 0.  Le dernier chiffre des lignes de position indique le saut retenu
      entre les deux blocs qui composent le modele.

[Pour desactiver la gestion et l'affichage des positions, il suffit de
recompiler les repertoires P_BLOCS et P_BLOCS+DELTA en desactivant les
deux flags qui sont en tete des makefiles respectifs. Le flag NB_OCCS
active ou non l'affichage du nombre total d'occurrences, et AFF_OCCS
active ou non l'affichage de chacune des positions des occurrences.]

  2 - le fichier "example.out.stat" qui contient les resultats de 100 shufflings
      ayant pour but de determiner si les modeles trouves sont representatifs
      ou non. On obtient:

    STATS SUR LE NOMBRE D'OCCURRENCES PAR SEQUENCE
    Modele           %right #right  %shfl.  #shfl.  Sigma   Chi2    Z-score
    =======================================================================
    ATTGAC_TATAAT    4.43%	   47	0.49%	 5.24	2.17	34.22	19.28
    AGAAAA_TTTTTC    5.18%	   55	1.30%	13.85	3.50	25.42	11.77
    GAAAAA_TTTTTC    5.46%	   58	1.44%	15.30	3.78	25.76	11.29
    ...

    STATS SUR LE NOMBRE D'OCCURRENCES TOTAL
    Modele           #right #shfl.	Sigma   Chi2	Z-score
    =======================================================================
    ATTGAC_TATAAT       47	 5.28	2.18	33.30	19.14
    AGAAAA_TTTTTC       80	16.34	4.31	42.08	14.76
    GAAAAA_TTTTTC       89	19.05	5.03	45.30	13.90
    ...

    Les modeles sont tries selon le Z-score.
    Plus le Z-score est eleve, plus le modele est exceptionnellement
    represente dans la sequence originale.  S'il est negatif, le modele est
    sous-represente. S'il vaut MAX_INT il n'a pu etre evalue (le motif n'a
    pas ete trouve dans les sequences shufflees par exemple).
    Le Chi2, indique egalement la representativite du modele, sans signe
    evidemment: un Chi2 fort indique un modele tres sur ou sous-represente.
    Il faut donc se fier au signe du Z-score ou aux pourcentages pour statuer
    sur le Chi2. Les colonnes "right" donnent les pourcentages et nombre
    d'occurrence du motif dans le fichier 'fasta'. Les colonnes "shfl" donnent
    les memes valeurs dans les sequences shufflees. La colonne "Sigma" indique
    l'ecart-type.

    Ici, ATTGAC_TATAAT semble etre nettement sur-represente dans les sequences
    du fichier 'fasta'.
    


La meilleure tactique, quand on ne connait pas suffisament de criteres du
modele qu'on souhaite extraire, consiste a tatonner en jouant sur les
longueurs, quorum et erreurs, de facon a obtenir un nombre raisonnable de
modeles (si l'extraction produit 0 modeles ou enormement de modeles, c'est
que les criteres ne sont pas adaptes). Pendant cette etape de tatonnement,
on desactive la deuxieme phase d'evaluation en retirant les lignes
de la rubrique EVALUATION.
    Puis, une fois l'extraction correctement calibree, on peut lancer
l'evaluation seule a partir de ces donnees.

[Pour lancer une evaluation sans lancer l'extraction, lancer "smile -x
<fichier parametre>" avec un fichier de parametres complet.]

["smile -g <nb>" affiche un fichier de parametres type pour l'extraction
de modeles a <nb> blocs.]

Si tu as besoin d'aide, de documentation ou references,  ou bien que tu
souhaites me signaler un bug:
=> lama -AT- prism.uvsq.fr

A bientot,
Laurent

REFERENCES:
** Pour des details algorithmiques sur SMILE:
[0] M.F. Sagot. "Spelling approximate repeated or common motifs using a suffix
tree." In C.L. Lucchesi and A.V. Moura, editors, LATIN'98: Theoretical
Informatics, Lecture Notes in Computer Sciences, 111-127. Springer-Verlag, 1998.

[1] L. Marsan and M.-F. Sagot. "Extracting structured motifs using a suffix-tree
- Algorithms and application to promoter consensus identification."
Proceedings RECOMB'2000, Tokyo. ACM Press.

** Pour une revue des differents algorithmes d'extraction existants, et
** des applications de SMILE:
[2] A. Vanet, L. Marsan, and M.-F. Sagot. "Promoter sequences and algorithmical
methods for identifying them." Research in Microbiology 150 (1999): 779-799.

[3] A. Vanet, L. Marsan, A. Labigne, and M.-F. Sagot. "Inferring regulatory
elements from a whole genome. An analysis of the sigma 80 family of promoter
signals." J. Mol. Biol. 297(2) (2000): 335-353.
